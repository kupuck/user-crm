import { MongoClient } from 'mongodb';
import config from 'config';

const url = config.get('mongoUri');
const mongoClient = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });

const mongodbClient = new Promise((resolve, reject) => {
    try {
        mongoClient.connect((error, client) => error ? reject(error) : resolve(client));
    } catch (error) {
        console.log(error);
    }
});

export default mongodbClient;