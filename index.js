import express from 'express';
import config from 'config';
import fs from 'fs';
import cors from 'cors';
import usersRouter from './routes/users.routes.js';

const app = express();
const PORT = config.get('port') || 5000;

app.use(express.json({ extented: true }));
app.use(cors());
app.use('/api/users', usersRouter);

app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`));