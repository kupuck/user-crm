import mongodbClient from "../components/mongodbClient.js";
import crypto from 'crypto';

class User {
    async init() {
        this.client = await mongodbClient;
        this.db = this.client.db('testing');
        this.collection = this.db.collection('users');
    }

    async getByParams(params) {
        await this.init();
        let condition = {};
        if (params.query) {
          condition.$or = [
            { name: { $regex: `.*${params.query}.*`, '$options' : 'i' } },
            { email: { $regex: `.*${params.query}.*`, '$options' : 'i' } },
            { company: { $regex: `.*${params.query}.*`, '$options' : 'i' } },
          ];
        }
        if (params.filters && params.filters.length) {
          condition.$and = [];
          params.filters.forEach(filter => {
            if (filter.field === 'isActive') {
              condition.$and.push({ [filter.field]: filter.firstValue });
            } else {
              condition.$and.push({ [filter.field]: { $gte: filter.firstValue, $lt: filter.secondValue } });
            }
          });
        }
        const fullCondition = (params.query && params.filters && params.filters.length) ? { $and: [condition] } : condition;
        const sort = params.sort ? { [params.sort.field]: params.sort.sortOrder } : { _id: 1 };
        const skip = (params.page - 1) * 10;
        const users = await this.collection.find(fullCondition)
          .limit(10)
          .skip(skip)
          .sort(sort)
          .toArray();
        const total = await this.collection.count(fullCondition);
        return { users, total };
    }

    async add(user) {
      await this.init();
      await this.collection.insertOne(user);
    }

    async bulkLoad(users) {
      await this.init();
      let objectsForBulkWrite = [];
      // let objectsForInsertMany = [];
      // let bulk = this.collection.initializeUnorderedBulkOp();
      // additional loop for generating million records, because in users array about 100k items
      for (let i = 0; i < 10; i++) {
        for (let key = 0; key < users.length; key++) {
          let object = users[key];
          object.name = `${object.name} ${i}${key}`;
          object.email = `${object.email} ${i}${key}`;
          object.isActive = object.isActive == 'True' ? true : false;
          object._id = crypto.createHash('md5').update(object.name).digest('hex');
          object.age = object.age && typeof object.age === 'string' ? parseInt(object.age.replace( /^\D+/g, '')) : 55;
          object.balance = object.balance && typeof object.balance === 'string' ? parseInt(object.balance.replace( /^\D+/g, '')) : 5000;
          objectsForBulkWrite.push({ insertOne: { document: object } });
          objectsForInsertMany.push(object);
          // BULKOP DOES NOT WORKING
          // bulk.insert(object);
          // if (key % 1000 == 0) {
          //   await bulk.execute((e, result) => { 
          //     console.log([e, result], "test");
          //   });
          //   bulk = this.collection.initializeUnorderedBulkOp();
          // }
        }

        // const response = await this.collection.insertMany(objectsForInsertMany, { ordered: false });
        // objectsForInsertMany = [];
        const response = await this.collection.bulkWrite(objectsForBulkWrite);
        console.log(`${i + 1}00k inserted`, "test");
        objectsForBulkWrite = [];
      }
    }
}

export default User;