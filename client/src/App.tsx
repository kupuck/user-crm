import React, { Fragment, useEffect } from 'react';
import Navbar from './components/layout/Navbar';
import UserPage from './components/pages/UserPage';
import AddUserBtn from './components/user/AddUserBtn';
import AddUserModal from './components/user/AddUserModal';
import M from 'materialize-css';
import 'materialize-css';

const App = () => {
  useEffect(() => {
    M.AutoInit();
  }, []);

	return (
    <Fragment>
      <Navbar />
      <UserPage />
      <AddUserBtn />
      <AddUserModal />
    </Fragment>
	);
};

export default App;