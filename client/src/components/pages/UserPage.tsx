import React, { FC, Fragment, useState, useEffect } from 'react';
import axios from 'axios';
import M from 'materialize-css';
import Loader from '../layout/Loader';
import SearchBar from '../user/SearchBar';
import Filters from '../user/Filters';
import UserList from '../user/UserList';
import Pagination from '../user/Pagination';
import AddFilterModal from '../user/AddFilterModal';
import { IUser, IUserPageFilter, IUserPageSort } from '../../types/types';

interface IUserPageResponse {
  status: boolean,
  message: string,
  users: IUser[],
  page: number,
  total: number
}

const UserPage: FC = () => {
  const [users, setUsers] = useState<IUser[]>([]);
  const [page, setPage] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const [searchQuery, setSearchQuery] = useState<string>('');
  const [sort, setSort] = useState<IUserPageSort | null>(null);
  const [filters, setFilters] = useState<IUserPageFilter[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const setSearchQueryWrapper = (query: string): any => setSearchQuery(query);

  const setSortWrapper = (listSort: IUserPageSort): any => setSort(listSort);

  const setPageWrapper = (pageNumber: number): any => setPage(pageNumber);

  const setFiltersWrapper = (filter: IUserPageFilter): any => {
    if (filters) {
      let exist = false;
      filters.forEach(f => f.field === filter.field ? exist = true : null);
      if (!exist) {
        setFilters(state => { return [...state, filter] });
      } else {
        let newFilters: IUserPageFilter[] = [];
        filters.forEach(f => newFilters.push(f.field === filter.field ? filter : f));
        setFilters(newFilters);
      }
    } else {
      setFilters([filter]);
    }
  };

  const removeFilterWrapper = (filter: IUserPageFilter): any => {
    if (filters) {
      let newFilters: IUserPageFilter[] = [];
      filters.forEach(f => f.field !== filter.field ? newFilters.push(f) : null);
      setFilters(newFilters);
    }
  };

  useEffect(() => {
    fetchUsers();
    console.log(sort, "test");
    
  }, [searchQuery, sort, filters, page]);


  async function fetchUsers() {
    setLoading(true);
    try {
      const params = JSON.stringify({ sort, filters, query: searchQuery, page });
      const response = await axios.get<IUserPageResponse>('http://localhost:5000/api/users', { params });
      if (response.status === 200 && response.data.status) {
        setUsers(response.data.users);
        setTotal(response.data.total);
        setPage(response.data.page);
      } else {
        M.toast({ html: response.data.message })
      }
      setLoading(false);
    } catch (error) {
      M.toast({ html: error });
      setLoading(false);
    }
  };

  return (
    <Fragment>
      { loading ? <Loader /> : ''}
      <div className="container">
        <SearchBar onChange={ setSearchQueryWrapper } />
        <Filters filters={ filters } removeFilter={ removeFilterWrapper } />
        { !loading ? [
          <Pagination page={page} total={total} setCurrentPage={setPageWrapper} />,
          <UserList users={ users } onChangeSort={ setSortWrapper } sortParams={sort} />,
          <Pagination page={page} total={total} setCurrentPage={setPageWrapper} />
        ] : '' }
      </div>
      <AddFilterModal addFilter={ setFiltersWrapper } />
    </Fragment>
  );
};

export default UserPage;