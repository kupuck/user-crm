import React, { useState, FC } from 'react';
import axios from 'axios';
import M from 'materialize-css';
import { IDefaultResponse } from '../../types/types';


const Navbar: FC = () => {
  const [loading, setLoading] = useState<boolean>(false);

  const loadData = async () => {
    setLoading(true);
    const response = await axios.post<IDefaultResponse>('http://localhost:5000/api/users/load');
    if (response.status === 200) {
      M.toast({ html: 'Users successfully uploaded' });
    } else {
      M.toast({ html: 'Internal server error' });
    }
    setLoading(false);
  };

  const customLoader = () => {
    return (
    <div className="preloader-wrapper small active va-middle">
      <div className="spinner-layer spinner-green-only">
        <div className="circle-clipper left">
          <div className="circle"></div>
        </div>
        <div className="gap-patch">
          <div className="circle"></div>
        </div>
        <div className="circle-clipper right">
          <div className="circle"></div>
        </div>
      </div>
    </div>
    )
  };

  return (
    <nav>
      <div className="nav-wrapper">
        <a href="#!" className="brand-logo">Organic Soft</a>
        <ul className="right hide-on-med-and-down">
          <li>
            <a href="#!" onClick={ () => !loading ? loadData() : null }>
              { loading ? customLoader() : [<i className="material-icons left">cloud_download</i>, 'Load users'] }
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;