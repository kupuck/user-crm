import React, { FC } from 'react';

const AddUserBtn: FC = () => {
  return (
    <div className="fixed-action-btn">
      <a href="#add-user-modal" className="btn-floating btn-large modal-trigger">
        <i className="material-icons">person_add</i>
      </a>
    </div>
  );
};

export default AddUserBtn;