import React, { FC } from 'react';
import { IUserPageFilter } from '../../types/types';

interface FilterItemProps {
  filter: IUserPageFilter
}

const FilterItem: FC<FilterItemProps> = ({ filter }) => {
  return (
    <div className="chip">
      { filter.field.toUpperCase() }
    </div>
  );
};

export default FilterItem;