import React, { FC } from 'react';

interface SearchBarProps {
  onChange: (query: string) => any
};

const SearchBar: FC<SearchBarProps> = ({ onChange }) => {

  return (
    <div className="row">
      <div className="input-field col s12">
        <i className="material-icons prefix">search</i>
        <input 
          id="icon_prefix"
          type="text"
          onChange={e => onChange(e.target.value)} />
        <label htmlFor="icon_prefix">Search</label>
      </div>
    </div>
  );
};

export default SearchBar;