import React, { FC } from 'react';
import { useState } from 'react';
import { IUser } from '../../types/types';

interface IUserPageSort {
  field: string,
  sortOrder: number
}

interface UserListProps {
  users: IUser[],
  sortParams: IUserPageSort | null,
  onChangeSort: (listSort: IUserPageSort) => any
}

const UserList: FC<UserListProps> = ({ users, onChangeSort, sortParams }) => {
  const [field, setField] = useState<string>(sortParams ? sortParams.field : '');
  const [sortOrder, setSortOrder] = useState<number>(sortParams ? sortParams.sortOrder : -1);

  const changeSort: any = (field: string) => {
    const newSortOrder:number = sortOrder === 1 ? -1 : 1;
    console.log([field, newSortOrder], "test");
    setSortOrder(newSortOrder);
    setField(field);
    onChangeSort({ field, sortOrder: newSortOrder });
  }

  const iconClasses = 'material-icons prefix';
  const thClasses = '';
  
  return (
    <div className="row">
      <div className="col s12">
        <table className="striped centered">
          <thead>
            <tr className="cursor-pointer">
              <th className={ field === 'name' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('name') }
              >
                Name <i className={ field === 'name' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
              <th className={ field === 'email' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('email') }
              >
                Email <i className={ field === 'email' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
              <th className={ field === 'gender' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('gender') }
              >
                Gender <i className={ field === 'gender' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
              <th className={ field === 'age' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('age') }
              >
                Age <i className={ field === 'age' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
              <th className={ field === 'address' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('address') }
              >
                Address <i className={ field === 'address' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
              <th className={ field === 'company' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('company') }
              >
                Company <i className={ field === 'company' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
              <th className={ field === 'phone' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('phone') }
              >
                Phone <i className={ field === 'phone' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
              <th className={ field === 'balance' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('balance') }
              >
                Balance <i className={ field === 'balance' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
              <th className={ field === 'isActive' ? `blue-text text-darken-2 ${thClasses}` : thClasses }
                onClick={ () => changeSort('isActive') }
              >
                Active <i className={ field === 'isActive' ? (sortOrder === 1 ? `${iconClasses} rotated` : iconClasses) : iconClasses }>sort</i>
              </th>
            </tr>
          </thead>
          <tbody>
            { users.map(user => (
              <tr key={ user._id }>
                <td>{ user.name }</td>
                <td>{ user.email }</td>
                <td>{ user.gender }</td>
                <td>{ user.age }</td>
                <td>{ user.address }</td>
                <td>{ user.company }</td>
                <td>{ user.phone }</td>
                <td>${ user.balance }</td>
                <td>{ user.isActive ? 'Active' : 'Inactive' }</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default UserList;