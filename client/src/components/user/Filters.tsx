import React, { FC } from 'react';

interface IUserPageFilter {
  field: string,
  firstValue: number | boolean,
  secondValue?: number
}

interface FiltersProps {
  filters: IUserPageFilter[],
  removeFilter: (filters: IUserPageFilter) => any
}

const Filters: FC<FiltersProps> = ({ filters, removeFilter }) => {
  return (
    <div className="row">
      <div className="col">
        <a href="#add-filter-modal" className="waves-effect waves-light btn modal-trigger">Add filter</a>
      </div>
      <div className="col s4">
        { filters.map((filter, key) => (
            <div key={ key } className="chip">
              { filter.field }
              <i className="close material-icons"
                onClick={ () => removeFilter(filter) }>close</i>
            </div>
        )) }
      </div>
    </div>
  );
};

export default Filters;