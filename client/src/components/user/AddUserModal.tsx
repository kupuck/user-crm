import React, { FC, useState } from 'react';
import axios from 'axios';
import M from 'materialize-css';
import { IUser, IDefaultResponse } from '../../types/types';

const AddUserModal: FC = () => {
  const [user, setUser] = useState<IUser>({
    name: '',
    email: '',
    address: '',
    eyeColor: '',
    gender: '',
    age: 0,
    phone: '',
    company: '',
    balance: 0,
    isActive: true
  });

  const validation = () => {
    const errors: string[] = [];
    if (!user.name) {
      errors.push('Name value is invalid');
    }
    if (!user.email) {
      errors.push('Email value is invalid');
    }
    if (!user.address) {
      errors.push('Address value is invalid');
    }
    if (!user.eyeColor) {
      errors.push('Eye color value is invalid');
    }
    if (!user.gender) {
      errors.push('Gender value is invalid');
    }
    if (!user.age) {
      errors.push('Age value is invalid');
    }
    if (!user.phone) {
      errors.push('Phone value is invalid');
    }
    if (!user.balance && user.balance !== 0) {
      errors.push('Balance value is invalid');
    }
    if (!user.company) {
      errors.push('Company value is invalid');
    }
    return errors;
  };

  const onSubmit = async () => {
    const validationResult: string[] = validation();
    if (!validationResult.length) {
      const response = await axios.post<IDefaultResponse>('http://localhost:5000/api/users', { user });
      console.log(response);
    } else {
      validationResult.forEach(error => M.toast({ html: error }));
    }
  };

  return (
    <div id="add-user-modal" className="modal">
      <div className="modal-content">
        <h4>New User</h4>
        <div className="row">
          <div className="input-field">
            <input type="text" name="name" value={user.name} onChange={ e => setUser(state => { return {...state, name: e.target.value}}) }/>
            <label htmlFor="name" className="active">Name</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <input type="email" name="email" value={user.email} onChange={ e => setUser(state => { return {...state, email: e.target.value}}) }/>
            <label htmlFor="email" className="active">Email</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <input type="text" name="address" value={user.address} onChange={ e => setUser(state => { return {...state, address: e.target.value}}) }/>
            <label htmlFor="address" className="active">Address</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <input type="text" name="eyeColor" value={user.eyeColor} onChange={ e => setUser(state => { return {...state, eyeColor: e.target.value}}) }/>
            <label htmlFor="eyeColor" className="active">Eye Color</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <select name="gender" defaultValue={user.gender} onChange={ e => setUser(state => { return {...state, gender: e.target.value}}) }>
              <option value="" disabled>Choose your gender</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
            <label>Materialize Select</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <input type="number" name="age" value={user.age} onChange={ e => setUser(state => { return {...state, age: parseInt(e.target.value)}}) }/>
            <label htmlFor="age" className="active">Age</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <input type="text" name="phone" value={user.phone} onChange={ e => setUser(state => { return {...state, phone: e.target.value}}) }/>
            <label htmlFor="phone" className="active">Phone</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <input type="text" name="company" value={user.company} onChange={ e => setUser(state => { return {...state, company: e.target.value}}) }/>
            <label htmlFor="company" className="active">Company</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <input type="number" name="balance" value={user.balance} onChange={ e => setUser(state => { return {...state, balance: parseInt(e.target.value)}}) }/>
            <label htmlFor="balance" className="active">Balance</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <label>
              <input type="checkbox" name="isActive" checked={user.isActive} onChange={ e => setUser(state => { return {...state, isActive: e.target.checked}}) } />
              <span>Active</span>
            </label>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <a href="#!" onClick={onSubmit} className="modal-close waves-effect btn">Save</a>
      </div>
    </div>
  )
};

export default AddUserModal;