import React, { FC, useState } from 'react';
import { IUserPageFilter } from '../../types/types';
import M from 'materialize-css';

interface AddFilterProps {
  addFilter: (filter: IUserPageFilter) => any
};

const AddFilterModal: FC<AddFilterProps> = ({ addFilter }) => {
  const [filter, setFilter] = useState<IUserPageFilter>({
    field: '',
    firstValue: false
  });

  return (
    <div id="add-filter-modal" className="modal">
      <div className="modal-content">
        <h4>New Filter</h4>
        <div className="row">
          <div className="input-field">
            <select name="field"
              defaultValue={ filter.field }
              onChange={ e => setFilter(state => { return { ...state, field: e.target.value } }) }
            >
              <option value="" disabled>Choose field</option>
              <option value="age">Age</option>
              <option value="balance">Balance</option>
              <option value="isActive">Active</option>
            </select>
            <label>Field</label>
          </div>
        </div>
        { filter.field ? (
          filter.field === 'age' || filter.field === 'balance' ? (
            <div className="row">
              <div className="input-field">
                <input type="text"
                  id="firstValue"
                  value={ typeof filter.firstValue == "boolean" ? '0' : filter.firstValue.toString() }
                  onChange={ e => setFilter(state => { return { ...state, firstValue: parseInt(e.target.value) } }) }
                />
                <label htmlFor="firstValue" className="active">From</label>
              </div>
            </div>
          ) : (
            <div className="row">
              <div className="input-field">
                <label>
                  <input type="checkbox"
                    name="isActive"
                    checked={ !!filter.firstValue }
                    onChange={ e => setFilter(state => { return { ...state, firstValue: !!e.target.checked } }) }
                  />
                  <span>Active</span>
                </label>
              </div>
            </div>
          )
        ) : '' }
        { filter.field && (filter.field === 'age' || filter.field === 'balance') ? (
            <div className="row">
              <div className="input-field">
                <input type="text"
                  id="secondValue"
                  value={ filter.secondValue ? filter.secondValue.toString() : '' }
                  onChange={ e => setFilter(state => { return { ...state, secondValue: parseInt(e.target.value) } }) }
                />
                <label htmlFor="secondValue" className="active">To</label>
              </div>
            </div>
        ) : '' }
      </div>
      <div className="modal-footer">
        <a href="#!"
          className="modal-close waves-effect btn"
          onClick={ () => {
            if (filter.field) {
              addFilter(filter);
              setFilter({ field: '', firstValue: false });
            } else {
              M.toast({ html: 'Choose some filter' });
            }
          } }
        >Save</a>
      </div>
    </div>
  )
};

export default AddFilterModal;