import React, { FC, Fragment } from 'react';

interface PaginationProps {
  page: number,
  total: number,
  setCurrentPage: (pageNumber: number) => any
}

const Pagination: FC<PaginationProps> = ({ page, total, setCurrentPage }) => {
  const totalPages = total / 10;

  const render = () => {
    let html: any[] = [];
    for (let pageIterator = page; pageIterator < totalPages && html.length < 8; pageIterator++) {
      html.push(
        <li key={ pageIterator } className={ pageIterator === page ? 'active' : 'waves-effect' }>
          <a href="#!" onClick={ (e) => setCurrentPage(pageIterator) }>{ pageIterator }</a>
        </li>
      );
    }
    return html;
  };
  
  if (total) {
    return (
      <ul className="pagination">
        <li className={ page - 1 === 0 ? 'disabled' : 'waves-effect' }>
          <a href="#!" onClick={ () => page - 1 !== 0 ? setCurrentPage(page - 1) : null }>
            <i className="material-icons">chevron_left</i>
          </a>
        </li>
        { render() }
        <li className={ page + 1 >= totalPages ? 'disabled' : 'waves-effect' }>
          <a href="#!" onClick={ () => page + 1 < totalPages ? setCurrentPage(page + 1) : null }>
            <i className="material-icons">chevron_right</i>
          </a>
        </li>
      </ul>
    );
  }
  return <Fragment />
};

export default Pagination;