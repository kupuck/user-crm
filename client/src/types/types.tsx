export interface IUser {
  _id?: string,
  name: string,
  email: string,
  address: string,
  eyeColor: string,
  gender: string,
  age: number,
  phone: string,
  company: string,
  balance: number,
  isActive: boolean
}

export interface IUserPageFilter {
  field: string,
  firstValue: number | boolean,
  secondValue?: number
}

export interface IUserPageSort {
  field: string,
  sortOrder: number
}

export interface IDefaultResponse {
  status: boolean,
  message: string
}