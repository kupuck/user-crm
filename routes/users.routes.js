import { Router } from 'express';
import User from '../models/User.js';
import fs from 'fs';

const usersRouter = Router();

usersRouter.get('/', async (req, res) => {
  const params = JSON.parse(req.query[0]);
  try {
    const userModel = new User();
    const data = await userModel.getByParams(params);
    const response = {
      users: data.users,
      page: params.page,
      total: data.total,
      status: true,
      message: 'Successfull',
    };
    res.status(200).json(response);
  } catch (error) {
    const response = { status: false, message: 'Internal error'};
    res.status(500).json(response);
  }
});

usersRouter.post('/', async(req, res) => {
  const { user } = req.body;
  try {
    const userModel = new User();
    await userModel.add(user);
    res.status(200).json({ status: true, message: 'User successfully created' });
  } catch (error) {
    res.status(200).json({ status: false, message: error });
  }
});

usersRouter.post('/load', async(req, res) => {
  const data = JSON.parse(fs.readFileSync('./data.json'));
  try {
    const userModel = new User();
    const response = await userModel.bulkLoad(data.objects);
    res.status(200).json({ status: true, message: 'Users successfully loaded' });
  } catch (error) {
    console.log(error);
    res.status(500).json({ status: false, message: error });
  }
});

export default usersRouter;